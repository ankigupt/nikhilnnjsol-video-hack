import subprocess
import os
import execute

def main():
    input_file  = os.path.dirname(os.path.realpath(__file__))+"/../input/20140726_040000_ps.h264"
    output_file_mp4 = os.path.dirname(os.path.realpath(__file__))+"/../output/H264toMp4.mp4"
    output_file_mkv = os.path.dirname(os.path.realpath(__file__))+"/../output/H264toMkv.mkv"
    if not (os.path.isfile(output_file_mp4)):
        command =["ffmpeg","-r","30","-i",input_file,"-c","copy",output_file_mp4]
        execute.execute(command)
    if not (os.path.isfile(output_file_mkv)):
        command =["ffmpeg","-r","30","-i",input_file,"-c","copy",output_file_mkv]
        execute.execute(command)

if __name__ == '__main__':
    main()

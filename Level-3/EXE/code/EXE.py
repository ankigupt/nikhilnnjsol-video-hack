import subprocess
import os
import execute

def main():
    input_file  = os.path.dirname(os.path.realpath(__file__))+"/../input/Brixton.exe"
    input_dir = os.path.dirname(os.path.realpath(__file__))+"/../input/"
    output_dir = os.path.dirname(os.path.realpath(__file__))+"/../output/"
    if not (os.path.isdir(input_dir+"_Brixton.exe.extracted/")):
        command =["binwalk","-e",input_file,"-C",input_dir]
        execute.execute(command)
    else:
        print "Binary already extracted"
    valid_data=["4490F90","3BD8EFE","2F50BC9"]
    for data in valid_data:
        path=output_dir+data+"-output.mp4"
        in_path=input_dir+"_Brixton.exe.extracted/"+data+".7z"
        if not (os.path.isfile(path)):
            command =["ffmpeg","-r","30","-i",in_path,"-vcodec","libx264",path]
            execute.execute(command)
        else:
            print "MP4 conversion already done for"+data


if __name__ == '__main__':
    main()
